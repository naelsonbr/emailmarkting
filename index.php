<?php

$array = array();
$arrayName = array();

if (isset($_FILES['image'])) {
    if (count($_FILES['image']['tmp_name']) > 0) {

        for ($q = 0; $q < count($_FILES['image']['tmp_name']); $q++) {
            $fileName = md5($_FILES['image']['tmp_name'][$q] . time() . rand(0, 999)) . '.jpg';

            move_uploaded_file($_FILES['image']['tmp_name'][$q], 'img/' . $fileName);

            $array[$q] = 'http://vipcriativo.com/up/img/' . $fileName;
            $arrayName[$q] = $_FILES['image']['name'][$q];
        }


        $message = '
        <html>
        <body>
         <h1 style="
         width: 100%;
         height: 50px;
         color: #2d2d2d;
         border-radius: 15px;
         text-align: center;
         font-family: Arial, Helvetica, sans-serif
         ">Links das Imagens</h1>
        <ol>';

        for ($i = 0; $i < count($array); $i++)
            $message .= '<li><a href="' . $array[$i] . '">' . $arrayName[$i] . '</img></li>';

        $message .= '</ol></body></html>';

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.1' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        if(isset($_POST['email'])){
            mail($_POST['email'], "Imagem de Pecas", $message, $headers);
        }
        //mail("naelson.g.saraiva@gmail.com", "Imagem de pecas", $message, $headers);
    }

}


?>


<!doctype html>
<html lang="pt-br">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Upload de Imagens</title>

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 m-auto text-center">
            <form method="post" enctype="multipart/form-data" action="index.php" style="margin-top: 200px;">
                        <input type="email" class="form-control" name="email" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1">
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" name="image[]"  class="custom-file-input" id="inputGroupFile04" multiple>
                        <label class="custom-file-label" for="inputGroupFile04" style="font-size: 14px !important;">Selecione 3 Image</label>
                    </div>
                    
                    </div>
                    <div class="input-group-append">
                        <button class="btn btn-danger" type="submit">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

<script type="application/javascript">
    $(function () {
        $("button[type='submit']").click(function () {
            let $fileUpload = $("input[type='file']");
            document.getElementById('count-selected').innerHTML = $fileUpload.get(0).files.length;
            if (parseInt($fileUpload.get(0).files.length) > 3) {
                alert("Quantidade maior que permitido!");
            }
        });
    });
</script>

</body>
</html>

